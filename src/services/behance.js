/**
 * Created by wesley on 15/07/17.
 */
import axios from 'axios'

const client = axios.create({
  baseURL: 'https://behance.net/v2/',
  headers: {
    'Authorization': 'Basic d2VzbGV5Y2d1aXJyYTpvY21uY2UxOTk1',
    'Host': 'www.behance.net',
    'X-Target-URI': 'http://www.behance.net',
    'Connection': 'Keep-Alive'
  }
})

axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    console.log('401 error')
  }
  return Promise.reject(error)
})

export default client
