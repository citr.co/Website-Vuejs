/**
 * Created by wesley on 15/07/17.
 */
import http from './wp'

export function getPosts () {
  return http.get('posts', {params: {per_page: 1, order: 'desc'}})
    .then(response => {
      return response.data[0]
    })
}

export function getFeaturedImage (id) {
  return http.get('media/', {params: {parent: id}})
    .then(response => {
      return response.data[0]
    })
}

export function getAuthor (id) {
  return http.get('users/', {params: {include: id}})
    .then(response => {
      return response.data[0]
    })
}
