import Vue from 'vue'
import Router from 'vue-router'
import Post from '@/components/Post'
import Portfolio from '@/components/Portfolio'
import Ux from '@/components/Ux'
import Cloud from '@/components/Cloud'
import Mvp from '@/components/Mvp'
import Privacy from '@/components/Privacy'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Post',
      component: Post
    },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio
    },
    {
      path: '/ux',
      name: 'UX',
      component: Ux
    },
    {
      path: '/devops',
      name: 'Cloud',
      component: Cloud
    },
    {
      path: '/minimum-viable-product',
      name: 'Mvp',
      component: Mvp
    },
    {
      path: '/privacy-policy',
      name: 'Privacy',
      component: Privacy
    }
  ]
})
