/**
 * Created by wesley on 15/07/17.
 */
import axios from 'axios'

const client = axios.create({baseURL: 'https://blog.citr.co/wp-json/wp/v2/'})

axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    console.log('401 error')
  }
  return Promise.reject(error)
})

export default client
